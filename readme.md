# Makerspace Code of Conduct

## Our Pledge

In the interest of fostering an open and welcoming environment, we as
members and directors, pledge to making participation in our makerspace and
our community a harassment-free experience for everyone, regardless of:

* age
* body
* size
* disability
* ethnicity
* sex characteristics
* gender identity and expression
* education
* socio-economic status 
* nationality
* personal appearance
* race
* religion
* sexual identity and orientation
* level of experience
* chosen craft

## Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or
  advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

## Our Responsibilities

Makerspace directors are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Makerspace directors have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any member for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

## Safety

### Report all Injuries/Incidents
* Immediately report ALL accidents, injuries, or incidents to a makerspace director.
* Immediately discontinue use of the tool(s) or equipment if it becomes unsafe, damaged, or is not working properly; notify a makerspace director.

### Safe Conduct Agreement

* Be aware of locations of first aid, fire, and safety equipment.
* Use the space and equipment safely and leave the space and equipment in clean and working condition.
* Work and behave in a way that protects your own safety and the safety of others.
* Children under the age of 18 years must be accompanied and supervised by an adult.
* Ask for help when you are uncertain how to use equipment.
* Never use a tool unless you’ve been trained to use it safely.
* Do not work when tired or in a hurry.

### Use Protective Gear & Dress Right

* Do not wear loose-fitting clothing around moving or rotating machinery.
* Remove ties, jewelry, lanyards, etc., especially around moving or rotating machinery.
* Tie back long hair.
* Wear suitable gloves when handling hot objects or sharp-edged items.
* Wear suitable eye and face protection when using machines that throw debris or blind you instantly 

### Use Tools Correctly

* Use tools how they are designed to be used.
* Never use a broken tool. Report any broken tools or machinery to a makerspace director immediately.
* Do not remove tools from the Makerspace.
* Never walk away from a tool that is still on. 
* Never tamper with a tool’s safety features. Operate machinery according to recommended procedures and with safety guards in place, as applicable. 

### Clean Up

* Clean up after yourself; leave the area clean and tidy.
* Clean and return all tools to where you got them.
* Shut off and unplug machines when cleaning. 
* Never use a rag near moving machinery. 
* Use a brush, hook, or a special tool to remove chips, shavings, etc., from the work area. Never use your hands. 
* Keep fingers clear of the point of operation of machines by using special tools or devices, such as push sticks, hooks, pliers, etc. 
* Keep the floor around machinery clean, dry, and free from trip hazards. 
* Clean up spills immediately and put a chair or cone over them if they are wet enough to cause someone to slip.
* Be respectful of the physical space your project takes up and make sure it doesn't infringe on another member's project.

## Scope

This Code of Conduct applies both within the makerspace and in public spaces
when an individual is representing the makerspace or its community. Examples of
representing a makerspace or community include using an official makerspace e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a makerspace may be
further defined and clarified by makerspace directors.

## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the makerspace directors at [INSERT EMAIL ADDRESS]. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The makerspace directors are
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Makerspace directors who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the makerspace's leadership.

## Attribution

This Code of Conduct is adapted from the Contributor Covenant, version 1.4,
available at https://www.contributor-covenant.org/version/1/4/code-of-conduct.html